<?php

namespace Empu\Exporter\Commands;

use Carbon\Carbon;
use FilesystemIterator;
use Illuminate\Support\Facades\File;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;

class CleanOldDirs extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'exporter:clean';

    /**
     * @var string The console command description.
     */
    protected $description = 'Clean temporary import/export directories';

    /**
     * Execute the console command.
     * @return void
     */
    public function handle()
    {
        $keepSpan = (int) $this->option('keep-span');

        $this->clean($keepSpan);
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['keep-span', 'k', InputOption::VALUE_OPTIONAL, 'Keep span in days.', 3],
        ];
    }

    protected function clean(int $keepSpan)
    {
        $today = Carbon::today();
        $keepDate = $today->subDays($keepSpan);
        $dirs = $this->scanDirs();

        foreach ($dirs as $fileInfo) {
            $dirName = $fileInfo->getBasename();
            $createDate = Carbon::createFromFormat('Ymd', $dirName);
            $pathName = $fileInfo->getPathname();
            
            if ($createDate->lte($keepDate)) {
                $this->info('Deleting ' . $pathName);
                File::deleteDirectory($pathName);
            }
        }

        $this->info('Temp dir has been cleaned');
    }

    protected function scanDirs()
    {
        $managedDirs = [temp_path('public/exports'), temp_path('public/imports')];
        $dirCollection = collect();

        foreach ($managedDirs as $dirPath) {
            $dirs = new FilesystemIterator($dirPath);
    
            foreach ($dirs as $fileInfo) {
                if ($fileInfo->isDir()) {
                    $dirCollection->push($fileInfo);
                }
            }
        }

        return $dirCollection;
    }
}
