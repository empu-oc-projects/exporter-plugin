<?php

namespace Empu\Exporter;

use Backend;
use System\Classes\PluginBase;

/**
 * Exporter Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Exporter',
            'description' => 'No description provided yet...',
            'author'      => 'Wuri Nugrahadi',
            'icon'        => 'icon-floppy-o'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {
        $this->registerConsoleCommand('exporter.clean', Commands\CleanOldDirs::class);
    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Empu\Exporter\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'empu.exporter.some_permission' => [
                'tab' => 'Exporter',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'exporter' => [
                'label'       => 'Exporter',
                'url'         => Backend::url('empu/exporter/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['empu.exporter.*'],
                'order'       => 500,
            ],
        ];
    }

    public function registerSchedule($schedule)
    {
        $schedule->call('exporter:clean')->daily();
    }
}
