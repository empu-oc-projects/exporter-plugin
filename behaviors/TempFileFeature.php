<?php

namespace Empu\Exporter\Behaviors;

use Illuminate\Support\Facades\Storage;
use October\Rain\Support\Facades\File;

trait TempFileFeature
{
    public function exportFilePath(string $title): string
    {
        $basePath = 'exports/public/' . date('Ymd');
        $filePath = $basePath . '/' . str_slug($title, '_') . '.xlsx';

        $this->prepareExportDir($basePath);

        return $filePath;
    }

    public function getFileUrl(string $filePath): string
    {
        return Storage::url($filePath);
    }

    public function prepareExportDir(string $path = ''): void
    {
        $path = storage_path($path);

        File::isDirectory($path) or File::makeDirectory($path, 0777, true, true);
    }
}