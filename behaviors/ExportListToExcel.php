<?php

namespace Empu\Exporter\Behaviors;

use Box\Spout\Common\Type;
use Box\Spout\Writer\Style\StyleBuilder;
use Box\Spout\Writer\WriterFactory;
use Exception;
use Illuminate\Support\Facades\Log;
use October\Rain\Extension\ExtensionBase;
use October\Rain\Support\Facades\File;

/**
 * ExportListToExcel
 */
class ExportListToExcel extends ExtensionBase
{
    protected $controller;

    public function __construct($controller)
    {
        $this->controller = $controller;

        // Implement behavior if not already implemented
        if (!$controller->isClassExtendedWith('Backend.Behaviors.ListController')) {
            throw new \Exception('Backend.Behaviors.ListController must be implemented in controller.');
        }
    }

    public function index_onExportExcel()
    {
        $title = post('title', 'export') . ' - ' . time();
        $definition = post('definition', 'list');

        $basePath = 'public/exports/' . date('Ymd');
        $filePath = $basePath . '/' . str_slug($title, '_') . '.xlsx';
        $this->prepareTempDir($basePath);
        $fileName = temp_path($filePath);

        try {
            $titleStyle = (new StyleBuilder())
                ->setFontSize(16)
                ->setFontBold()
                ->build();
            $headerStyle = (new StyleBuilder())
                ->setFontBold()
                ->build();
            $writer = WriterFactory::create(Type::XLSX);
            $writer->openToFile($fileName);
            $writer->addRowWithStyle([$title], $titleStyle);

            $list = $this->controller->makeList($definition);
            $model = $list->prepareQuery();
            $data = $model->get();

            $writer->addRowWithStyle($this->headerRow($list), $headerStyle);

            foreach ($data as $record) {
                $writer->addRow($this->recordRow($list, $record));
            }

            $writer->close();
        } catch (Exception $e) {
            Log::error($e);

            throw new Exception('Terjadi galat saat proses menghasilkan berkas!');
        }

        return [
            'url' => $this->getUrlTempFile($filePath)
        ];
    }

    protected function headerRow($list)
    {
        $columns = $this->getColumns($list);
        $headerValues = [];

        foreach ($columns as $col) {
            array_push($headerValues, $list->getHeaderValue($col));
        }

        return $headerValues;
    }

    protected function recordRow($list, $record)
    {
        $columns = $this->getColumns($list);
        $recordValues = [];

        foreach ($columns as $col) {
            array_push($recordValues, $list->getColumnValue($record, $col));
        }

        return $recordValues;
    }

    protected function getColumns($list)
    {
        $columns = $list->getColumns();
        $onlyColumnsString = post('columns');
        $onlyColumns = $onlyColumnsString ? explode(',', $onlyColumnsString) : false;

        return $onlyColumns ? array_only($columns, $onlyColumns) : $columns;
    }

    public function getUrlTempFile($filePath)
    {
        return url('storage/temp/' . $filePath);
    }

    public function prepareTempDir($path = '')
    {
        $path = temp_path($path);

        File::isDirectory($path) or File::makeDirectory($path, 0777, true, true);
    }
}
